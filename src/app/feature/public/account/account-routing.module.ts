import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent } from './account.component';


const routes: Routes = [
  {
    path: '', component: AccountComponent,
    children: [
      {
        path: '', loadChildren: () => import('@publicFeature/account/left-screen/left-screen.module')
          .then(leftScreen => leftScreen.LeftScreenModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
