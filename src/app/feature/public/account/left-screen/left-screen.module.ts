import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftScreenRoutingModule } from './left-screen-routing.module';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SignInFormComponent } from './sign-in/sign-in-form/sign-in-form.component';
@NgModule({
  declarations: [SignInComponent, SignUpComponent, SignInFormComponent],
  imports: [
    CommonModule,
    LeftScreenRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ]
})
export class LeftScreenModule { }
