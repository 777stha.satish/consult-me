import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { RouteConstant } from '@sharedConstant/route-constant';

const { signInRoute, signUpRoute } = RouteConstant;

const routes: Routes = [
  {
    path: signInRoute, component: SignInComponent
  },
  {
    path: signUpRoute, component: SignUpComponent
  },
  {
    path: '', pathMatch: 'full', redirectTo: signInRoute
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeftScreenRoutingModule { }
