import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { OAuth2AccessToken } from './model/oauth2-access-token';
import { LoginService } from './service/login.service';
import { localStorageConstant } from '@sharedConstant/local-storage-constant';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  subSink = new SubSink();

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.loginService.buildForm();
  }

  onLogin(): void {
    const accessTokenKey = localStorageConstant.key.accessToken;
    this.subSink.add(this.loginService.login(this.loginForm.value)
      .subscribe((oAuth2AccessToken: OAuth2AccessToken) => {
        localStorage.setItem(accessTokenKey, oAuth2AccessToken.access_token);
        this.router.navigate(['main']);
      }));
  }

  ngOnDestroy() {
    this.subSink.unsubscribe();
  }
}
