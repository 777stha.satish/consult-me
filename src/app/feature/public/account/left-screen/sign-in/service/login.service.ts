import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '@env/environment';
import { ApiURLConstant } from '@sharedConstant/api-url-constant';
import { Observable } from 'rxjs';
import { Authenticator } from '../model/authenticator';
import { OAuth2AccessToken } from '../model/oauth2-access-token';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseURL = environment.baseURL;

  constructor(private httpClient: HttpClient, private fb: FormBuilder) { }

  buildForm(): FormGroup {
    return this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(authenticator: Authenticator): Observable<OAuth2AccessToken> {
    const loginURL = ApiURLConstant.public + ApiURLConstant.login;
    return this.httpClient
      .post(this.baseURL + loginURL, authenticator) as Observable<OAuth2AccessToken>;
  }
}
