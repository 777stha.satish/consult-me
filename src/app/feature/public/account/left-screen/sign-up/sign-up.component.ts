import { Component, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { CompareFn, WithFn, ExtendedField } from '@sharedComponent/form-control-wrap/model/field-control';
import { AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';
import { DynamicFormService } from '@sharedService/dynamic-form/dynamic-form.service';
import { UserRegistrationService } from './service/user-registration.service';
import { User } from './model/user';
import { FormValueTo } from './mapper/form-value-to';
import { FieldKeys } from '@enum/field-keys';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, OnDestroy {
  regFormGroup: FormGroup;
  regSubSink = new SubSink();
  restFields: ExtendedField[];
  firstName: ExtendedField;
  lastName: ExtendedField;

  constructor(private dynamicFormService: DynamicFormService, private registrationService: UserRegistrationService) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm(): void {
    this.regSubSink.sink = this.dynamicFormService.formGroup({
      [FieldKeys.firstName]: null,
      [FieldKeys.lastName]: null,
      [FieldKeys.email]: null,
      [FieldKeys.signUpUsername]: null,
      [FieldKeys.password]: [null, { controlType: 'password', autoComplete: 'new-password' }],
      [FieldKeys.confirmPassword]: [null, { controlType: 'password', compareWith: compare(FieldKeys.password) }]
    }, 'blur')
      .subscribe(({ fields, formGroup }) => {
        [this.firstName, this.lastName, ...this.restFields] = fields;
        this.regFormGroup = fields && fields.length && formGroup;
      });
  }

  onRegister(): void {
    if (this.regFormGroup.valid) {
      this.regSubSink.sink = this.registrationService.register(FormValueTo.user(this.regFormGroup.value))
        .subscribe((user: User) => {
        });
    }
  }

  ngOnDestroy() {
    this.regSubSink.unsubscribe();
  }
}

export const compare: CompareFn =
  (sourceKey: string): WithFn => (target: AbstractControl, callBack: (sourceKey?: string) => AbstractControl): ValidationErrors => {
    const sourceControl: AbstractControl = callBack(sourceKey);
    const sourceValue = sourceControl.value;
    const targetValue = target.value;
    if (targetValue && sourceValue !== targetValue) {
      return {
        notEqual: 'Password and confirm password must be equal',
      };
    }
    return {
      notEqual: null,
    };
  };
