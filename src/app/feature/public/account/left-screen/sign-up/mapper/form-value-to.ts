import { RegistrationForm } from '../model/registration-form';
import { User } from '../model/user';

export class FormValueTo {

  static user(value: RegistrationForm): User {
    if (!value) {
      return null;
    }

    return {
      username: value.username,
      email: value.email,
      password: value.password,
      id: 0,
      userProfile: {
        firstName: value.firstName,
        lastName: value.lastName,
        id: 0
      }
    };
  }
}
