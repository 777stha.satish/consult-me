import { Identity } from 'src/app/shared/model/identity';

export interface UserProfile {
  id: number;
  firstName: string;
  lastName: string;
}
