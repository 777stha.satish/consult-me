import { Identity } from 'src/app/shared/model/identity';
import { UserProfile } from './user-profile';

export interface User {
  id: number;
  username: string;
  password: string;
  email: string;
  userProfile: UserProfile;
  enabled?: boolean;
}
