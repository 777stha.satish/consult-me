import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { ApiURLConstant } from '@sharedConstant/api-url-constant';
import { Observable } from 'rxjs';
import { User } from '../model/user';
@Injectable({
  providedIn: 'root'
})
export class UserRegistrationService {

  private basePath = environment.oauth;
  private usersPath = this.basePath + ApiURLConstant.users;
  private registrationFullPath = this.usersPath + ApiURLConstant.register;

  constructor(private httpClient: HttpClient) { }

  register(user: User): Observable<User> {
    return this.httpClient.post<User>(this.registrationFullPath, user);
  }
}
