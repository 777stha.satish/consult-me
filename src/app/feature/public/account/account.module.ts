import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';

import { AccountComponent } from './account.component';
import { SocialLoginComponent } from './right-screen/social-login/social-login.component';
import { RightScreenComponent } from './right-screen/right-screen.component';
import { LeftScreenRendererComponent } from './right-screen/left-screen-renderer/left-screen-renderer.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [AccountComponent,
    SocialLoginComponent, RightScreenComponent, LeftScreenRendererComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    SharedModule
  ]
})
export class AccountModule { }
