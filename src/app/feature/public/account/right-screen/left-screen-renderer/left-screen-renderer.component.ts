import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { RouteConstant } from '@sharedConstant/route-constant';
import { ToggleAccountService } from '@core/layout/public/header/service/toggle-account.service';
import { SubSink } from 'subsink';
@Component({
  selector: 'app-left-screen-renderer',
  templateUrl: './left-screen-renderer.component.html',
  styleUrls: ['./left-screen-renderer.component.scss']
})
export class LeftScreenRendererComponent implements OnInit, OnDestroy {
  isRegister: boolean;
  private subSink = new SubSink();

  constructor(private accountToggle: ToggleAccountService, private router: Router) { }

  ngOnInit() {
    this.checkRoute();
    this.accountToggle.toggle();
  }

  checkRoute() {
    this.subSink.sink = this.accountToggle.listen()
      .subscribe(() => {
        if (this.router.url === RouteConstant.signInFullRoute) {
          this.isRegister = false;
        } else {
          this.isRegister = true;
        }
      });
  }

  routeTo() {
    const navigate = this.router.navigate.bind(this.router);
    if (!this.isRegister) {
      navigate([RouteConstant.signUpFullRoute]);
    } else {
      navigate([RouteConstant.signInFullRoute]);
    }
    this.isRegister = !this.isRegister;
  }

  renderActionName(): string {
    return this.isRegister ? 'Sign in' : 'Sign up';
  }

  description(): string {
    const signUpDesc = `Not a member? Click on the below button to sign up`;
    const signInDesc = `Already a member? Click on the below button to sign in`;
    return this.isRegister ? signInDesc : signUpDesc;
  }

  ngOnDestroy() {
    this.subSink.unsubscribe();
  }
}
