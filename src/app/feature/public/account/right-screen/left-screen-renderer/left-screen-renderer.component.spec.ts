import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftScreenRendererComponent } from './left-screen-renderer.component';

describe('LeftScreenRendererComponent', () => {
  let component: LeftScreenRendererComponent;
  let fixture: ComponentFixture<LeftScreenRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftScreenRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftScreenRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
