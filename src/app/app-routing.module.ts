import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstant } from '@sharedConstant/route-constant';
import { CanLoadPublicService } from '@publicLayout/service/can-load/can-load-public.service';
import { CanLoadSecuredService } from '@securedLayout/service/can-load/can-load-secured.service';

const { homeRoute, securedPageInitials } = RouteConstant;

const routes: Routes = [
  {
    path: homeRoute, loadChildren: () => import('@layout/public/public.module').then(module => module.PublicModule),
    canLoad: [CanLoadPublicService],

  },
  {
    path: securedPageInitials, loadChildren: () => import('@layout/secured/secured.module').then(module => module.SecuredModule),
    canLoad: [CanLoadSecuredService]
  },
  {
    path: '', pathMatch: 'full', redirectTo: homeRoute
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
