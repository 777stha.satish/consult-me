import { Component, OnDestroy, OnInit } from '@angular/core';
import { FieldWithValidatorService } from '@core/service/field-with-validator/field-with-validator.service';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'consult-me';
  isFieldLoaded: boolean;
  constructor(private activeFieldService: FieldWithValidatorService) { }
  ngOnInit(): void {
    this.getFields();
  }

  private getFields(): void {
    this.activeFieldService.getFields()
      .pipe(
        takeWhile(() => this.isFieldLoaded)
      )
      .subscribe(() => {
        this.isFieldLoaded = true;
      });
  }

  ngOnDestroy(): void {
  }
}
