import { Pipe, PipeTransform } from '@angular/core';
import { sentenceCaseBySplit } from '@util/string-manipulation';

@Pipe({
  name: 'sentenceCaseBySplit'
})
export class SentenceCaseBySplitPipe implements PipeTransform {

  transform(value: string, delimiter: string): string {
    return sentenceCaseBySplit(value, delimiter);
  }
}
