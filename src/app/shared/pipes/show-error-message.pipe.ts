import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { FieldValidator } from '@sharedModel/field-validator';
import { delimiters } from '@sharedConstant/delimiters';
import { ExtendedField } from '../component/form-control-wrap/model/field-control';

@Pipe({
  name: 'showErrorMessage'
})
export class ShowErrorMessagePipe implements PipeTransform {
  transform(field: ExtendedField, errors: ValidationErrors, dirty: boolean, touched: boolean): string {
    return this.getErrorMessage(field, errors, dirty, touched);
  }

  getErrorMessage(field: ExtendedField, errors: ValidationErrors, dirty: boolean, touched: boolean): string {
    if (errors) {
      for (const error of Object.keys(errors)) {
        const invalidField = this.getInvalidField(error, field);
        if (invalidField && (dirty || touched)) {
          return invalidField.messageResponse.message;
        } else if (dirty || touched) {
          return errors[error];
        }
      }
    }
    return '';
  }

  private getInvalidField(error: string, field: ExtendedField): FieldValidator {
    return field.fieldValidators.find(fieldValidator => fieldValidator.validator.type
      .replace(delimiters.validatorType, '').toLowerCase() === error);
  }
}
