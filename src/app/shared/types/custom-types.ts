
export type Active = 'active' | '';
export const ACTIVE: Active = 'active';

export type ValidatorDataType = 'INTEGER' | 'DECIMAL' | 'STRING' | 'BOOLEAN';
export const INTEGER: ValidatorDataType = 'INTEGER';
export const DECIMAL: ValidatorDataType = 'DECIMAL';
export const STRING: ValidatorDataType = 'STRING';
export const BOOLEAN: ValidatorDataType = 'BOOLEAN';
export type BasicInputType = 'password' | 'text' | 'email' | 'url' | 'tel';


export type AutoComplete = 'on' | 'off' | 'new-password' | 'current-password';

export type AccountAction = 'SIGN_IN' | 'SIGN_UP';
export const SIGN_IN: AccountAction = 'SIGN_IN';
export const SIGN_UP: AccountAction = 'SIGN_UP';
