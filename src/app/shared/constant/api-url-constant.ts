export const ApiURLConstant = {
  public: '/public',
  login: '/login',
  menu: '/menu',
  fieldValidators: '/fieldValidators',
  messageResponse: '/messageResponse',
  validatorValue: '/validatorValue',
  fields: '/fields',
  users: '/users',
  register: '/add',
  params: {
    enabled: 'enabled',
    messageResponseId: 'messageResponseId',
    newMessage: 'newMessage',
    validatorValueId: 'validatorValueId',
    newValue: 'newValue',
    value: 'value'
  }
};
