export const RouteConstant = {
  securedPageInitials: 'main',
  homeRoute: 'home',
  signUpRoute: 'sign-up',
  signInRoute: 'sign-in',
  signInFullRoute: '/home/sign-in',
  signUpFullRoute: '/home/sign-up',
  homeFullRoute: '/main/home'
};
