export  const  DeepClone = <T> (target: T): T => {
  if (target) {
    return JSON.parse(JSON.stringify(target));
  }
  return target;
};

