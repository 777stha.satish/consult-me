
export const matchedValues = <T, S>(target: T[], source: S[], callBack: (targetValue: T, sourceValue: S) => boolean): T[] => {
  checkFilterCriteria(target, source);
  return target.filter((targetValue: T) =>
    source.findIndex((sourceValue: S) => callBack(targetValue, sourceValue)) !== -1
  );
};

export const unMatchedValues = <T, S>(target: T[], source: S[], callBack: (targetValue: T, sourceValue: S) => boolean): T[] => {
  checkFilterCriteria(target, source);
  return target.filter((targetValue: T) =>
    source.findIndex((sourceValue: S) => callBack(targetValue, sourceValue)) === -1
  );
};

export const mapByMatching = <T, S>(target: T[], source: S[],
                                    found: (targetValue: T, sourceValue: S) => boolean,
                                    notFound?: (sourceValue?: S) => T): T[] => {
  checkFilterCriteria(target, source);
  return source.map(sourceValue => {
    const foundItem = target.find(targetValue => found(targetValue, sourceValue));
    if (foundItem) {
      return foundItem;
    }
    return notFound && notFound(sourceValue) || null;
  }).filter(value => value);
};

const checkFilterCriteria = <T, S>(target: T[], source: S[]) => {
  if (!target && !source) {
    throw new Error('Target and source are not defined');
  } else if (!target) {
    throw new Error('Target is not defined');
  } else if (!source) {
    throw new Error('Source is not defined');
  }
};
