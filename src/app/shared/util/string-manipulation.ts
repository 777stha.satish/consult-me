export const sentenceCase = (value: string): string => {
  if (value) {
    return `${value[0].toUpperCase()}${value.substr(1).toLowerCase()}`;
  }
  return value;
};

export const ignoreCaseIncludes = (target: string, source: string): boolean => {
  return target.toLowerCase().includes(source.toLowerCase());
};

export const sentenceCaseBySplit = (value: string, delimiter: string): string => {
  return sentenceCase(`${value.split(delimiter)[0]} ${value.split(delimiter)[1] || ''}`);
};

export const replaceToInteger = (value: string): string => {
  return value.replace(/\D/g, '');
};

export const replaceToDecimal = (value: string): string => {
  if (value === '.') {
    return '0.';
  }
  return (value.match(/^\d*\.?\d*/g) || [''])[0];
};
