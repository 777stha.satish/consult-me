import { NgModule } from '@angular/core';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormControlWrapComponent } from './component/form-control-wrap/form-control-wrap.component';
import { BaseInputControlComponent } from './component/form-control-wrap/base-input-control/base-input-control.component';
import { InputControlComponent } from './component/form-control-wrap/input-control/input-control.component';
import { TypeAheadControlComponent } from './component/form-control-wrap/type-ahead-control/type-ahead-control.component';
import { FieldErrorComponent } from './component/field-error/field-error.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SentenceCaseBySplitPipe } from '@pipes/sentence-case-by-split.pipe';
import { ShowErrorMessagePipe } from '@pipes/show-error-message.pipe';
import { CustomFieldValidatorDirective } from './directives/custom-field-validator/custom-field-validator.directive';
import { HandleFormSubmitDirective } from '@directives/handle-form-submit/handle-form-submit.directive';
import { HeaderColorTogglerDirective } from './directives/header-color-toggler/header-color-toggler.directive';
import { TitleTagComponent } from './component/title-tag/title-tag.component';
import { NavBarLogoComponent } from './component/nav-bar-logo/nav-bar-logo.component';
import { MenuTogglerDirective } from './directives/menu-toggler/menu-toggler.directive';
import { MenuTogglerComponent } from './component/menu-toggler/menu-toggler.component';
@NgModule({
  declarations: [FormControlWrapComponent, BaseInputControlComponent,
    InputControlComponent, TypeAheadControlComponent,
    FieldErrorComponent, SentenceCaseBySplitPipe,
    ShowErrorMessagePipe, CustomFieldValidatorDirective,
    HandleFormSubmitDirective, HeaderColorTogglerDirective,
    TitleTagComponent, NavBarLogoComponent,
    MenuTogglerDirective, MenuTogglerComponent],
  imports: [
    NgbTypeaheadModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  exports: [FormControlWrapComponent, BaseInputControlComponent,
    InputControlComponent, TypeAheadControlComponent,
    FieldErrorComponent, CustomFieldValidatorDirective,
    SentenceCaseBySplitPipe, HandleFormSubmitDirective,
    HeaderColorTogglerDirective, TitleTagComponent,
    NavBarLogoComponent, MenuTogglerDirective,
    MenuTogglerComponent]
})
export class SharedModule { }
