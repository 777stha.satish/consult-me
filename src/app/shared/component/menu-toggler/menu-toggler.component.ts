import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-toggler',
  templateUrl: './menu-toggler.component.html',
  styleUrls: ['./menu-toggler.component.scss']
})
export class MenuTogglerComponent implements OnInit {
  @Input() sideBar: HTMLElement;

  constructor() { }

  ngOnInit(): void {
  }
}
