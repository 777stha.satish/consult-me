import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, NgControl } from '@angular/forms';
import { ExtendedField } from '../form-control-wrap/model/field-control';

@Component({
  selector: 'app-field-error',
  templateUrl: './field-error.component.html',
  styleUrls: ['./field-error.component.scss']
})
export class FieldErrorComponent implements OnInit {

  @Input() field: ExtendedField;
  control: AbstractControl;

  constructor(private ngControl: NgControl) { }

  ngOnInit() {
    this.control = this.ngControl.control;
  }

}
