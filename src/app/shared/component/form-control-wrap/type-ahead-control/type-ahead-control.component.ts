import { Component, EventEmitter, forwardRef, OnInit, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ignoreCaseIncludes } from '@util/string-manipulation';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { TypeAheadOption } from '../../../model/type-ahead-option';
import { BaseInputControlComponent } from '../base-input-control/base-input-control.component';

@Component({
  selector: 'app-type-ahead-control',
  templateUrl: './type-ahead-control.component.html',
  styleUrls: ['./type-ahead-control.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TypeAheadControlComponent),
      multi: true
    }
  ]
})
export class TypeAheadControlComponent extends BaseInputControlComponent implements OnInit {
  @Output() selectedItem = new EventEmitter<TypeAheadOption>();
  @Output() focusOut = new EventEmitter<FocusEvent>();

  ngOnInit() {
    this.field = this.field;
  }

  formatter = (option: TypeAheadOption) => option.displayValue || option;

  onSelectItem = (option: { item: TypeAheadOption; }) => {
    this.selectedItem.emit(option.item);
  };

  search = (text: Observable<string>) => {
    if (this.field.options && this.field.options.length > 0) {
      return text
        .pipe(
          debounceTime(400),
          map(value => {
            if (!value) {
              return [];
            } else {
              return this.field.options
                .filter(option => ignoreCaseIncludes(option.displayValue, value))
                .splice(0, 10);
            }
          })
        );
    }
    return [];
  };

  onFocusOut(event: FocusEvent): void {
    this.focusOut.emit(event);
  }
}
