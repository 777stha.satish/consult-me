import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAheadControlComponent } from './type-ahead-control.component';

describe('TypeAheadControlComponent', () => {
  let component: TypeAheadControlComponent;
  let fixture: ComponentFixture<TypeAheadControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeAheadControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAheadControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
