import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Field } from '../../../model/field';
import { AutoComplete, BasicInputType } from 'src/app/shared/types/custom-types';
import { TypeAheadOption } from '../../../model/type-ahead-option';

export interface Error {
  errorType: string;
  message: string;
  rejectedValue: string;
}

export type WithFn = (target: AbstractControl, callBack: (sourceKey?: string) => AbstractControl) => ValidationErrors;

export type CompareFn = (sourceKey: string) => WithFn;

interface CommonFieldAttributes {
  compareWith?: WithFn;
  disabled?: boolean;
}

export interface InputAttributes extends CommonFieldAttributes {
  autoComplete?: AutoComplete;
  controlType?: BasicInputType;
}

export type InputField = InputAttributes & Field;

export interface TypeAheadAttributes extends CommonFieldAttributes {
  options?: TypeAheadOption[];
}

export type TypeAheadField = TypeAheadAttributes & Field;

export type ExtendedField = InputField & TypeAheadField;

export type FieldAttribute = InputAttributes & TypeAheadAttributes;


export const defaultInputAttributes: InputAttributes = {
  autoComplete: 'off',
  controlType: 'text',
  disabled: false
};

export const defaultTypeAheadAttribute: TypeAheadAttributes = {
  disabled: false,
  options: []
};

export const defaultFieldAttribute: FieldAttribute = { ...defaultInputAttributes, ...defaultTypeAheadAttribute };

