import { Component, Input, OnInit } from '@angular/core';
import { DefaultValueAccessor } from '@angular/forms';
import * as sm from '@util/string-manipulation';
import { DECIMAL, INTEGER } from 'src/app/shared/types/custom-types';
import { ExtendedField } from '../model/field-control';
import { FieldValidator } from '../../../model/field-validator';
import { MAX_LENGTH, MIN_LENGTH } from '../../../model/validator';

@Component({
  selector: 'app-base-input-control',
  template: '',
})
export class BaseInputControlComponent extends DefaultValueAccessor implements OnInit {
  @Input() id: string;
  @Input() field: ExtendedField;

  maxLength = 500;
  minLength: number;
  inputValidation: boolean;

  ngOnInit(): void {
    this.setValidators();
  }

  setValidators(): void {
    if (this.field && this.field.fieldValidators && this.field.fieldValidators.length) {
      this.field.fieldValidators
        .forEach((value: FieldValidator) => {
          const validatorValue = value.validatorValue.value;
          switch (value.validator.type) {
            case MAX_LENGTH:
              this.maxLength = +validatorValue;
              break;
            case MIN_LENGTH:
              this.minLength = +validatorValue;
              break;
            default:
              break;
          }
        });
    }
  }

  onInput(event: InputEvent): void {
    if (this.field && this.field.validatorDataType) {
      const inputElement = event.target as HTMLInputElement;
      const inputValue = inputElement.value;
      if (this.field.validatorDataType === INTEGER) {
        inputElement.value = sm.replaceToInteger(inputValue);
      } else if (this.field.validatorDataType === DECIMAL) {
        inputElement.value = sm.replaceToDecimal(inputValue);
      }
    }
  }

  onBlur(value: InputEvent): void {
    this.onTouched();
    this.trimDot(value);
  }

  private trimDot(event: InputEvent): void {
    const inputElement = event.target as HTMLInputElement;
    const inputValue = inputElement.value;
    if (inputValue === '.' || inputValue.endsWith('.')) {
      inputElement.value = inputValue.substring(0, inputValue.length - 1);
    }
  }
}
