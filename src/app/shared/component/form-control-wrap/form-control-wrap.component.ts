import { Component, Input, OnInit } from '@angular/core';
import { ExtendedField } from './model/field-control';

@Component({
  selector: 'app-form-control-wrap',
  templateUrl: './form-control-wrap.component.html',
  styleUrls: ['./form-control-wrap.component.scss']
})
export class FormControlWrapComponent implements OnInit {
  @Input() for: string;
  @Input() field: ExtendedField;
  asteroid: string;

  constructor() { }

  ngOnInit() {
    this.setAsteroid();
  }

  setAsteroid(): void {
    this.asteroid = this.field && this.field.fieldValidators &&
      this.field.fieldValidators.find(value => value.validator.type === 'REQUIRED') && '*';
  }
}
