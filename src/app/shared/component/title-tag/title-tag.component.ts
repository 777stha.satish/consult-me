import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-title-tag',
  templateUrl: './title-tag.component.html',
  styleUrls: ['./title-tag.component.scss']
})
export class TitleTagComponent implements OnInit {
  @Input() title: string;
  @Input() orientation: 'left' | 'right';
  constructor() { }

  ngOnInit(): void {
  }

}
