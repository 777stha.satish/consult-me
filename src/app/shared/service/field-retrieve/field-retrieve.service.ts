import { Injectable } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import { Field } from '@sharedModel/field';
import { FieldValidator } from '@sharedModel/field-validator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FieldWithValidatorService } from 'src/app/core/service/field-with-validator/field-with-validator.service';
import { FieldKeysTo } from './mapper/field-keys-to';
import { FieldValidatorTo } from './mapper/field-validator-to';
@Injectable({
  providedIn: 'root'
})
export class FieldRetrieveService {

  constructor(private fieldService: FieldWithValidatorService) { }

  getFieldByKeys(...fieldKeys: string[]): Observable<Field[]> {
    return this.fieldService.getFromCache()
      .pipe(map(FieldKeysTo.respectiveFields(...fieldKeys)));
  }

  getValidatorsFrom(fieldValidators: FieldValidator[]): ValidatorFn[] {
    return fieldValidators && fieldValidators.length &&
      fieldValidators.map(FieldValidatorTo.validatorFn);
  }
}
