import { TestBed } from '@angular/core/testing';

import { FieldRetrieveService } from './field-retrieve.service';

describe('FieldRetrieveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FieldRetrieveService = TestBed.get(FieldRetrieveService);
    expect(service).toBeTruthy();
  });
});
