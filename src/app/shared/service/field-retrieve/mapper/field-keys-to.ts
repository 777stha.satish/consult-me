import { defaultField, Field } from '@sharedModel/field';
import { mapByMatching } from '@util/array-query';

export class FieldKeysTo {
  static respectiveFields = (...fieldKeys: string[]) => (fields: Field[]): Field[] => {
    if (fields && fields.length && fieldKeys) {
      return mapByMatching(fields, fieldKeys, (field, key) => field.fieldKey === key,
        (key) => {
          return {
            ...defaultField,
            fieldKey: key
          };
        });
    }
  };
}
