import { Validators } from '@angular/forms';
import { FieldValidator } from '@sharedModel/field-validator';
import { MAX_LENGTH, MAX_VALUE, MIN_LENGTH, MIN_VALUE, PATTERN, REQUIRED } from '@sharedModel/validator';

export class FieldValidatorTo {
  static validatorFn = (fieldValidator: FieldValidator) => {
    switch (fieldValidator.validator.type) {
      case MAX_LENGTH:
        return Validators.maxLength(+fieldValidator.validatorValue.value);
      case MIN_LENGTH:
        return Validators.minLength(+fieldValidator.validatorValue.value);
      case MAX_VALUE:
        return Validators.max(+fieldValidator.validatorValue.value);
      case MIN_VALUE:
        return Validators.min(+fieldValidator.validatorValue.value);
      case PATTERN:
        return Validators.pattern(fieldValidator.validatorValue.value);
      case REQUIRED:
        return Validators.required;
      default:
        break;
    }
  };
}
