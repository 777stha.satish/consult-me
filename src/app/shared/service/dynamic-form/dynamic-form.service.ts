import { Injectable } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Field } from '@sharedModel/field';
import { FieldValidator } from '@sharedModel/field-validator';
import { DeepClone } from '@util/clone';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { defaultFieldAttribute, ExtendedField, FieldAttribute } from '../../component/form-control-wrap/model/field-control';
import { FieldRetrieveService } from '../field-retrieve/field-retrieve.service';

@Injectable({
  providedIn: 'root'
})
export class DynamicFormService {

  constructor(private fieldRetrieve: FieldRetrieveService) { }

  formGroup(controls: ControlConfig, updateGroupOn?: 'blur' | 'submit' | 'change'): Observable<FieldsWithGroup> {
    const group = new FormGroup({}, { updateOn: updateGroupOn || 'change' });
    return this.fieldRetrieve.getFieldByKeys(...Object.keys(controls))
      .pipe(map(this.withFormGroup(group, controls)));
  }

  private withFormGroup = (group: FormGroup, controls: ControlConfig) =>
    (fields: Field[]): FieldsWithGroup => {
      if (fields) {
        const extendedFields = fields.map((field: Field) => {
          return this.addControl(group, field, field.fieldKey, controls);
        });
        const validFields = extendedFields.filter(field => field.name);
        return { fields: validFields, formGroup: group };
      }
      return { fields: [], formGroup: group };
    };

  private addControl(group: FormGroup, field: Field, fieldKey: string, controls: ControlConfig): ExtendedField {
    if (field && field.name) {
      return this.addVisibleControl(group, field, fieldKey, controls);
    } else {
      this.whenFieldIsNull(group, fieldKey, controls);
      return field;
    }
  }

  private newFormControl(value: string, fieldValidators?: FieldValidator[]) {
    if (fieldValidators) {
      return new FormControl(value, this.fieldRetrieve.getValidatorsFrom(fieldValidators));
    }
    return new FormControl(value);
  }

  private addVisibleControl(group: FormGroup, field: Field, fieldKey: string, controls: ControlConfig): ExtendedField {
    const extendedField = this.extraFieldInfo(field, controls, fieldKey);
    group.addControl(extendedField.name, this.newFormControl(extendedField.value, extendedField.fieldValidators));
    return extendedField;
  }

  private whenFieldIsNull(group: FormGroup, fieldKey: string, controls: ControlConfig): void {
    const control = controls[fieldKey];
    if (control instanceof FormArray) {
      group.addControl(fieldKey, control as FormArray);
    } else {
      group.addControl(fieldKey, this.newFormControl(control + ''));
    }
  }

  private extraFieldInfo(field: Field, controls: ControlConfig, fieldKey: string): ExtendedField {
    const newField = DeepClone(field);
    const control = controls[fieldKey];
    newField.visible = true;
    let fieldAttribute: FieldAttribute = defaultFieldAttribute;
    if (control instanceof Array) {
      newField.value = control[0];
      fieldAttribute = { ...fieldAttribute, ...control[1] };
    } else {
      newField.value = control as string;
    }
    return { ...newField, ...fieldAttribute };
  }
}

export interface FieldsWithGroup {
  fields: ExtendedField[];
  formGroup: FormGroup;
}

export interface ControlConfig {
  [key: string]: [string, FieldAttribute] | string | FormArray;
}
