import { Directive, HostListener, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appHeaderColorToggler]'
})
export class HeaderColorTogglerDirective implements OnInit {

  constructor(private elRef: ElementRef<HTMLElement>, private renderer: Renderer2) {
  }

  ngOnInit() {
  }

  @HostListener('document:scroll')
  onScroll(): void {
    if (document.scrollingElement.scrollTop > 20) {
      this.toggleStyle(true);
    } else {
      this.toggleStyle(false);
    }
  }


  private toggleStyle(hasChanged: boolean) {
    const navBar = this.elRef.nativeElement;
    const styleProp = this.getStyleProp(hasChanged);
    this.toggleClass(hasChanged, navBar);
    this.setStyle(navBar, styleProp.backgroundImage, styleProp.boxShadowColor);
  }

  private getStyleProp(hasChanged: boolean): { backgroundImage: string, boxShadowColor: string; } {
    let linearGradient = 'none';
    let color = 'white';
    if (hasChanged) {
      linearGradient = 'linear-gradient(to right,#cccbcb,#dbdada, #f7f7f7, #f5f5f5, #c3c3c3)';
      color = 'rgb(106, 106, 106)';
    }
    return {
      backgroundImage: linearGradient,
      boxShadowColor: color
    };
  }

  private setStyle(navBar: HTMLElement, backgroundImage: string, boxShadowColor: string): void {
    this.elRef.nativeElement.style.backgroundImage = backgroundImage;
    this.renderer.setStyle(navBar.querySelector('.round-bound'), 'box-shadow', `0px 0px 3px 0px ${boxShadowColor}`);
    this.renderer.setStyle(navBar.querySelector('.nav-item .sign-up'), 'box-shadow', `0px 0px 3px 0px ${boxShadowColor}`);
  }

  private toggleClass(hasChanged: boolean, navBar: HTMLElement) {
    if (hasChanged) {
      this.addClass(navBar);
    } else {
      this.removeClass(navBar);
    }
  }

  private addClass(navBar: HTMLElement): void {
    this.renderer.addClass(navBar.getElementsByClassName('navbar-brand')[0], 'scrolled');
    this.eachNavLink(navBar, navLink => this.renderer.addClass(navLink, 'scrolled'));
  }

  private removeClass(navBar: HTMLElement): void {
    this.renderer.removeClass(navBar.getElementsByClassName('navbar-brand')[0], 'scrolled');
    this.eachNavLink(navBar, navLink => this.renderer.removeClass(navLink, 'scrolled'));
  }

  eachNavLink(navBar: HTMLElement, callBack: (navLink: HTMLAnchorElement) => void): void {
    navBar.querySelectorAll('.nav-link').forEach((navLink: HTMLAnchorElement) => {
      callBack(navLink);
    });
  }
}
