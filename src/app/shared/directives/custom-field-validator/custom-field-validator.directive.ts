import { Directive, Input, OnInit, OnDestroy } from '@angular/core';
import { ExtendedField } from '@sharedComponent/form-control-wrap/model/field-control';
import { SubSink } from 'subsink';
import { ControlContainer, FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appCustomFieldValidator]'
})
export class CustomFieldValidatorDirective implements OnInit, OnDestroy {
  @Input() field: ExtendedField;
  targetSourceSubSink = new SubSink();

  constructor(private controlInstance: ControlContainer) {
  }

  ngOnInit() {
    const formGroup = this.controlInstance.control as FormGroup;
    this.checkCustomValidation(this.field, formGroup);
  }

  private checkCustomValidation(field: ExtendedField, formGroup: FormGroup): void {
    const targetControl = formGroup.get(field.fieldKey);
    let sourceControl: AbstractControl;
    if (field.compareWith) {
      field.compareWith(targetControl, sourceKey => {
        sourceControl = formGroup.controls[sourceKey];
        return sourceControl;
      });
      this.subscribeToTargetOrSource(field, targetControl, sourceControl);
    }
  }

  subscribeToTargetOrSource(field: ExtendedField, targetControl: AbstractControl, sourceControl: AbstractControl): void {
    this.targetSourceSubSink.sink = this.subscribeToSource(field, targetControl, sourceControl);
    this.targetSourceSubSink.sink = this.subscribeToTarget(field, targetControl, sourceControl);
  }

  subscribeToTarget(field: ExtendedField, targetControl: AbstractControl, sourceControl: AbstractControl): Subscription {
    return targetControl.valueChanges.subscribe(() => {
      this.detectErrorChange(sourceControl, targetControl, field);
    });
  }

  subscribeToSource(field: ExtendedField, targetControl: AbstractControl, sourceControl: AbstractControl): Subscription {
    return sourceControl.valueChanges.subscribe(() => {
      this.detectErrorChange(sourceControl, targetControl, field);
    });
  }

  detectErrorChange(sourceControl: AbstractControl, targetControl: AbstractControl, field: ExtendedField): void {
    const error: ValidationErrors | Error = this.validateControl(targetControl, sourceControl, field);
    const errorKey = error && Object.keys(error)[0];
    if (errorKey && error[errorKey]) {
      targetControl.setErrors({ ...targetControl.errors, ...error });
    } else if (errorKey && targetControl.errors) {
      const errors = { ...targetControl.errors };
      delete errors[errorKey];
      targetControl.setErrors({ ...errors });
    }
  }

  validateControl(targetControl: AbstractControl, sourceControl: AbstractControl, field: ExtendedField): Error | ValidationErrors {
    return field.compareWith(targetControl, () => sourceControl);
  }

  ngOnDestroy(): void {
    this.targetSourceSubSink.unsubscribe();
  }

}
