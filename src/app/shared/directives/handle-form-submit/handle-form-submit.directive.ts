import { Directive, HostListener, Optional } from '@angular/core';
import { AbstractControl, FormArray, FormGroup, FormGroupDirective } from '@angular/forms';

@Directive({
  selector: 'form>button[type=submit]'
})
export class HandleFormSubmitDirective {

  constructor(@Optional() private formInstance: FormGroupDirective) {
  }

  @HostListener('mouseup')
  markFormDirty(): void {
    if (this.formInstance) {
      this.markControlsDirty(this.formInstance.form.controls);
    }
  }

  private markControlsDirty(controls: { [key: string]: AbstractControl; } | AbstractControl): void {
    Object.keys(controls)
      .forEach(key => {
        if (controls[key] instanceof FormArray) {
          this.markFormArrayDirty(controls as { [key: string]: AbstractControl; }, key);
        } else {
          this.markControlDirty(controls, key);
        }
      });
  }

  private markControlDirty(controls: { [key: string]: AbstractControl; } | AbstractControl, key: string): void {
    if (controls instanceof AbstractControl) {
      controls.markAsDirty();
    } else {
      controls[key].markAsDirty();
    }
  }

  private markFormArrayDirty(controls: { [key: string]: AbstractControl; }, key: string): void {
    const formArray = controls[key] as FormArray;
    if (formArray.length) {
      formArray.controls.forEach(control => {
        if (control instanceof FormGroup) {
          this.markControlsDirty(control.controls);
        } else {
          this.markControlsDirty(control);
        }
      });
    }
  }
}
