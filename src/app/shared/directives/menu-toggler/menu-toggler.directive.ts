import { Directive, HostListener, Input, Renderer2, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appMenuToggler]'
})
export class MenuTogglerDirective {
  @Input() sideBar: HTMLElement;

  constructor(private elRef: ElementRef<HTMLElement>, private render: Renderer2) { }

  @HostListener('click')
  private onToggle(): void {
    this.toggleContentClass();
    this.toggleSideBarClass();
    this.toggleTogglerClass();
  }

  private toggleSideBarClass(): void {
    this.toggleClassOf(this.sideBar);
  }

  private toggleContentClass(): void {
    this.toggleClassOf(document.getElementById('content'));
  }

  private toggleTogglerClass(): void {
    this.toggleClassOf(this.elRef.nativeElement);
    this.toggleClassOf(this.elRef.nativeElement.parentElement);
  }

  private toggleClassOf(element: HTMLElement) {
    if (!this.hasMinify(element)) {
      this.render.addClass(element, 'minify');
    } else {
      this.render.removeClass(element, 'minify');
    }
  }

  private hasMinify(element: HTMLElement): boolean {
    return element.classList.contains('minify');
  }
}
