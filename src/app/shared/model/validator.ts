import { Identity } from 'src/app/shared/model/identity';
import { ValidatorDataType } from 'src/app/shared/types/custom-types';

export type validatorType = 'MAX_LENGTH' | 'MIN_LENGTH' | 'MAX_VALUE' | 'MIN_VALUE' | 'REQUIRED' | 'PATTERN';
export const MAX_LENGTH: validatorType = 'MAX_LENGTH';
export const MIN_LENGTH: validatorType = 'MIN_LENGTH';
export const MAX_VALUE: validatorType = 'MAX_VALUE';
export const MIN_VALUE: validatorType = 'MIN_VALUE';

export const REQUIRED: validatorType = 'REQUIRED';
export const PATTERN: validatorType = 'PATTERN';


export interface Validator extends Identity<number> {
  type: validatorType;
  dataType: ValidatorDataType;

}
