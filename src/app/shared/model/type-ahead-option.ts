export interface TypeAheadOption {
  displayValue: string;
  returnValue: string | number | object | boolean;
}
