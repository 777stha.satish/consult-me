import { MessageResponse } from './message-response';
import { Validator } from './validator';
import { ValidatorValue } from './validator-value';

export interface FieldCollection {
  messageResponses: MessageResponse[];
  validators: Validator[];
  validatorValues: ValidatorValue[];
}
