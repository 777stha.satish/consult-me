import { Identity } from 'src/app/shared/model/identity';

export interface MessageResponse  extends Identity<number> {
  code: number;
  message: string;
}

export const defaultMessageResponse: MessageResponse = {
  code: null,
  id: 0,
  message: ''
};
