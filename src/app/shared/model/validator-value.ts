import { Identity } from 'src/app/shared/model/identity';

export interface ValidatorValue extends Identity<number> {
  value: string;
}

export const defaultValidatorValue: ValidatorValue = {
  id: 0,
  value: null,
};
