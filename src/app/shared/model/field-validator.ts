import { MessageResponse, defaultMessageResponse } from './message-response';
import { Validator } from './validator';
import { ValidatorValue, defaultValidatorValue } from './validator-value';

export interface FieldValidator {
  id: number | string;
  messageResponse: MessageResponse;
  validator: Validator;
  validatorValue: ValidatorValue;
  status: boolean;
}

export const defaultFieldValidator: FieldValidator = {
  id: 0,
  messageResponse: defaultMessageResponse,
  validator: {
    id: 0,
    type: null,
    dataType: null
  },
  validatorValue: defaultValidatorValue,
  status: null
};
