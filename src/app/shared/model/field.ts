import { Identity } from 'src/app/shared/model/identity';
import { ValidatorDataType } from 'src/app/shared/types/custom-types';
import { FieldValidator } from './field-validator';


export interface Field extends Identity<number> {
  fieldKey: string;
  name: string;
  label: string;
  placeholder: string;
  status: boolean;
  validatorDataType: ValidatorDataType;
  visible: boolean; // To make some field invisible. For example validator value for required field(Boolean input type)
  fieldValidators: FieldValidator[];
  value?: string;
}

export const defaultField: Field = {
  fieldKey: '',
  fieldValidators: [],
  id: 0,
  label: '',
  name: '',
  visible: false,
  placeholder: '',
  validatorDataType: null,
  status: null
};
