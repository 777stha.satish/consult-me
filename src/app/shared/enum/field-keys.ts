

// Do not change or update this constants directly fetch it from field_key of the tbl_field table

export enum FieldKeys {
  fieldKey = 'fieldKey',
  fieldLabel = 'label',
  fieldName = 'name',
  fieldPlaceholder = 'placeholder',
  messageResponse = 'messageResponse',
  validatorValue = 'validatorValue',
  signUpUsername = 'registrationUsername',
  email = 'email',
  password = 'password',
  firstName = 'firstName',
  lastName = 'lastName',
  confirmPassword = 'confirmPassword'
}
