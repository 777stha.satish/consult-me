import { TestBed } from '@angular/core/testing';

import { FieldWithValidatorService } from './field-with-validator.service';

describe('FieldWithValidatorService', () => {
  let service: FieldWithValidatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FieldWithValidatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
