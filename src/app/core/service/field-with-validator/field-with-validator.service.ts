import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Field } from '@sharedModel/field';
import { FieldValidator } from '@sharedModel/field-validator';
import { ApiURLConstant } from '@sharedConstant/api-url-constant';
import { DeepClone } from '@util/clone';
import { BehaviorSubject, MonoTypeOperatorFunction, Observable } from 'rxjs';
import { mergeMap, tap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class FieldWithValidatorService {
  private basePath = environment.baseURL;
  private fieldURL = `${this.basePath}${ApiURLConstant.public}${ApiURLConstant.fields}`;
  private fields = new BehaviorSubject<Field[]>(null);
  constructor(private httpClient: HttpClient) { }

  getFields = (): Observable<Field[]> => {
    if (this.fields.value) {
      return this.getFromCache();
    }
    return this.requestActiveFields();
  };

  getFromCache = (): Observable<Field[]> => {
    return this.fields.asObservable();
  };

  requestActiveFields = (): Observable<Field[]> => {
    return this.httpClient.get<Field[]>(this.fieldURL)
      .pipe(mergeMap(this.refreshAndGet));
  };

  addField = (field: Field): void => {
    if (field && this.isFieldsValid()) {
      this.refreshCache([...this.fields.value, DeepClone(field)]);
    }
  };

  removeField = (field: Field): void => {
    if (field && this.isFieldsValid()) {
      this.refreshCache(this.fields.value.filter((oldField: Field) => oldField.id !== field.id));
    }
  };

  updateField = (updatedField: Field): void => {
    if (updatedField && this.isFieldsValid()) {
      this.refreshCache(this.fields.value.map((field: Field) =>
        updatedField.id === field.id ? updatedField : field
      ));
    }
  };

  isFieldsValid = (): boolean => {
    return this.fields && this.fields.value && (this.fields.value.length > 0);
  };

  tapUpdate = (): MonoTypeOperatorFunction<Field> => {
    return tap(this.updateField);
  };

  tapAdd = (addValidatorOnly?: boolean): MonoTypeOperatorFunction<Field> => {
    return tap((field: Field) => addValidatorOnly !== true ?
      this.addField(field) : this.addFieldValidators(field.id, field.fieldValidators));
  };

  tapOnDelete = (): MonoTypeOperatorFunction<Field> => {
    return tap((field: Field) => field.status ? this.addField(field) : this.removeField(field));
  };

  addFieldValidator = (fieldId: number, fieldValidator: FieldValidator): void => {
    const currentField = this.findFieldBy(fieldId);
    if (currentField) {
      currentField.fieldValidators = [...currentField.fieldValidators, fieldValidator];
    }
  };

  addFieldValidators = (fieldId: number, fieldValidators: FieldValidator[]): void => {
    const currentField = this.findFieldBy(fieldId);
    if (currentField) {
      const oldFieldValidators = DeepClone(currentField.fieldValidators.filter(fieldValidator => fieldValidator.id > 0)) || [];
      currentField.fieldValidators = [...oldFieldValidators, ...fieldValidators];
    }
  };

  removeFieldValidator = (fieldId: number, fieldValidator: FieldValidator): void => {
    const currentField = this.findFieldBy(fieldId);
    if (currentField) {
      currentField.fieldValidators = [...currentField.fieldValidators
        .filter((oldFieldValidator: FieldValidator) => oldFieldValidator.id !== fieldValidator.id)];
    }
  };

  updateFieldValidator = (fieldId: number, fieldValidator: FieldValidator): void => {
    const currentField = this.findFieldBy(fieldId);
    if (currentField) {
      currentField.fieldValidators = currentField.fieldValidators
        .map((value: FieldValidator) => value.id === fieldValidator.id ?
          DeepClone(fieldValidator) : value);
    }
  };

  tapUpdateFieldValidator = (fieldId: number): MonoTypeOperatorFunction<FieldValidator> => {
    return tap((fieldValidator: FieldValidator) =>
      this.updateFieldValidator(fieldId, fieldValidator));
  };

  tapDeleteFieldValidator = (fieldId: number): MonoTypeOperatorFunction<FieldValidator> => {
    return tap((fieldValidator: FieldValidator) => fieldValidator.status ?
      this.addFieldValidator(fieldId, fieldValidator) :
      this.removeFieldValidator(fieldId, fieldValidator));
  };

  private findFieldBy = (id: number): Field => {
    return this.fields.value.find(field => field.id === id);
  };

  private refreshAndGet = (fields: Field[]): Observable<Field[]> => {
    this.refreshCache(fields);
    return this.getFromCache();
  };

  private refreshCache = (fields: Field[]): void => {
    this.fields.next(DeepClone(fields));
  };
}
