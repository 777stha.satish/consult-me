import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecuredRoutingModule } from './secured-routing.module';
import { SecuredComponent } from './secured.component';
import { HeaderComponent } from './header/header.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SideBarUiComponent } from './side-nav/side-bar-ui/side-bar-ui.component';
import { MenuListComponent } from './side-nav/side-bar-ui/menu-list/menu-list.component';
import { MenuListUiComponent } from '@sideNav/side-bar-ui/menu-list/ui/menu-list-ui.component';

@NgModule({
  declarations: [SecuredComponent, HeaderComponent, SideNavComponent,
    BreadcrumbComponent, SideBarUiComponent, MenuListComponent,
    MenuListUiComponent],
  imports: [
    CommonModule,
    SecuredRoutingModule,
    SharedModule
  ]
})
export class SecuredModule { }
