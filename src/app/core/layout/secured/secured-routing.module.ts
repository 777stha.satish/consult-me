import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecuredComponent } from './secured.component';
import { RouteConstant } from '@sharedConstant/route-constant';

const { homeRoute } = RouteConstant;

const routes: Routes = [
  {
    path: '', component: SecuredComponent,
    children: [
      {
        path: homeRoute, loadChildren: () => import('@secureFeature/home/home.module').then(home => home.HomeModule)
      },
      {
        path: '', pathMatch: 'full', redirectTo: homeRoute
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecuredRoutingModule { }
