import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { Menu } from '../model/menu';
import { Active, ACTIVE } from '@sharedTypes/custom-types';

@Component({
  selector: 'app-menu-list-ui',
  templateUrl: './menu-list-ui.component.html',
  styleUrls: ['./menu-list-ui.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuListUiComponent implements OnInit {

  @Input() menus: Menu[] = [];
  @Input() activeMenu: Menu;

  @Output() menuItemClicked = new EventEmitter<string>();


  constructor() { }

  ngOnInit(): void {
  }

  activateMenu(menu: Menu): Active {
    if (menu.active) {
      this.activeMenu = menu;
      return ACTIVE;
    }
    return '';
  }


  setMenuActive(menu: Menu): void {
    this.activeMenu.active = false;
    menu.active = true;
    this.activeMenu = menu;
    this.menuItemClicked.emit(menu.path);
  }
}
