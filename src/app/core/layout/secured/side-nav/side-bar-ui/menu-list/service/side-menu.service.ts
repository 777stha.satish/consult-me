import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { ApiURLConstant } from '@sharedConstant/api-url-constant';
import { BehaviorSubject, Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';
import { Menu } from '../model/menu';

@Injectable({
  providedIn: 'root'
})
export class SideMenuService {

  private $menus = new BehaviorSubject<Menu[]>([]);

  private menuLoadedToCache: boolean;

  private baseURL = environment.baseURL;

  constructor(private httpClient: HttpClient, private router: Router) { }

  loadMenus(): Observable<Menu[]> {
    if (!this.menuLoadedToCache) {
      return this.requestMenu();
    }
    return this.loadFromCache();
  }

  private requestMenu(): Observable<Menu[]> {
    const url = `${this.baseURL}${ApiURLConstant.menu}`;
    const httpParams = new HttpParams()
      .set(ApiURLConstant.params.enabled, 'true');
    return this.httpClient.get<Menu[]>(url, { params: httpParams })
      .pipe(
        this.loadToCache,
        this.mapActiveMenu
      );
  }

  private loadFromCache(): Observable<Menu[]> {
    return this.$menus.asObservable()
      .pipe(
        this.mapActiveMenu
      );
  }

  private get loadToCache(): OperatorFunction<Menu[], Menu[]> {
    return map((menus: Menu[]) => {
      this.menuLoadedToCache = true;
      this.$menus.next(menus);
      return menus;
    });
  }

  private get mapActiveMenu(): OperatorFunction<Menu[], Menu[]> {
    return map((menus: Menu[]) => {
      menus.map((menu: Menu) => {
        menu.active = false;
        if (menu.path === this.router.url) {
          menu.active = true;
        }
        return menu;
      });
      return menus;
    });
  }
}
