import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuListUiComponent } from './menu-list-ui.component';

describe('MenuListUiComponent', () => {
  let component: MenuListUiComponent;
  let fixture: ComponentFixture<MenuListUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuListUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuListUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
