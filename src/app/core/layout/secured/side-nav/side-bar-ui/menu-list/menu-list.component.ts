import { Component, OnInit, Input } from '@angular/core';
import { SideMenuService } from '@sideNav/side-bar-ui/menu-list/service/side-menu.service';
import { Observable } from 'rxjs';
import { Menu } from './model/menu';
@Component({
  selector: 'app-menu-list',
  template: `<app-menu-list-ui [menus] ="menus$ | async" ></app-menu-list-ui>`
})
export class MenuListComponent implements OnInit {
  @Input() collapse: boolean;

  menus$: Observable<Menu[]>;

  constructor(private menuService: SideMenuService) { }

  ngOnInit(): void {
    this.initMenu();
  }

  initMenu(): void {
    this.menus$ = this.menuService.loadMenus();
  }

}
