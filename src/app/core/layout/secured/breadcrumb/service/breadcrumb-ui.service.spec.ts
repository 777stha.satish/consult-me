import { TestBed } from '@angular/core/testing';

import { BreadcrumbUiService } from './breadcrumb-ui.service';

describe('BreadcrumbUiService', () => {
  let service: BreadcrumbUiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BreadcrumbUiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
