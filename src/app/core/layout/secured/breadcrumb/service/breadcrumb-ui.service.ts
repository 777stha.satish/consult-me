import { Injectable } from '@angular/core';
import { Breadcrumb } from '../model/breadcrumb';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { sentenceCase } from '@util/string-manipulation';
import { RouteConstant } from '@sharedConstant/route-constant';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbUiService {
  private readonly baseURL = '/main';
  private readonly initialBreadcrumb: Breadcrumb = {
    name: 'Home',
    path: 'home',
    icon: 'fas fa-home'
  };

  constructor(private router: Router, private route: ActivatedRoute) { }

  getBreadcrumbs(): Observable<Breadcrumb[]> {
    const activatedRoute = this.lastRoute(this.route);
    return activatedRoute.params.pipe(
      map(this.mapToBreadCrumbs)
    );
  }

  private mapToBreadCrumbs = (param: Params): Breadcrumb[] => {
    let url = this.baseURL;
    const breadcrumbs: Breadcrumb[] = [this.initialBreadcrumb];
    this.getURLs().forEach((chunk) => {
      if (this.canAddToBreadcrumb(chunk, param)) {
        url = `${url}/${chunk}`;
        breadcrumbs.push(this.toBreadCrumb(chunk, url));
      }
    });
    return breadcrumbs;
  }

  private canAddToBreadcrumb(route: string, param: Params): boolean {
    return route !== RouteConstant.securedPageInitials && route !== RouteConstant.homeRoute && !Object.values(param).includes(route);
  }

  private toBreadCrumb(chunk: string, url: string) {
    return {
      name: sentenceCase(chunk.toUpperCase()).replace('-', ' '),
      path: url
    };
  }

  private getURLs() {
    return this.router.url
      .replace('#', '')
      .replace(/(\?[\D\d]*)/, '').split('/').filter(Boolean);
  }

  private lastRoute(route: ActivatedRoute): ActivatedRoute {
    let child = route.firstChild;
    if (child) {
      child = this.lastRoute(child);
    } else {
      child = route;
    }
    return child;
  }
}
