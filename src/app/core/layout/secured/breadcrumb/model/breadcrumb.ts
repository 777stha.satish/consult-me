export interface Breadcrumb {
  name: string;
  path: string;
  icon?: string;
}
