import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Breadcrumb } from './model/breadcrumb';
import { BreadcrumbUiService } from './service/breadcrumb-ui.service';
@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  breadcrumbs$: Observable<Breadcrumb[]>;

  constructor(private breadcrumbService: BreadcrumbUiService) { }

  ngOnInit(): void {
    this.initBreadcrumbs();
  }

  initBreadcrumbs() {
    this.breadcrumbs$ = this.breadcrumbService.getBreadcrumbs();
  }
}
