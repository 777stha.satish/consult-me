import { Injectable } from '@angular/core';
import { CanLoad, Router, Route, UrlTree } from '@angular/router';
import { localStorageConstant } from '@sharedConstant/local-storage-constant';
import { RouteConstant } from '@sharedConstant/route-constant';

const { key } = localStorageConstant;
const { accessToken } = key;
const { signInFullRoute } = RouteConstant;

@Injectable({
  providedIn: 'root'
})
export class CanLoadSecuredService implements CanLoad {
  constructor(private router: Router) { }
  canLoad(
    route: Route,
    segments: import('@angular/router').UrlSegment[]
  ):
    | boolean
    | UrlTree
    | import('rxjs').Observable<boolean | UrlTree>
    | Promise<boolean | import('@angular/router').UrlTree> {
    return this.isLoggedIn();
  }

  isLoggedIn(): boolean | UrlTree {
    if (localStorage.getItem(accessToken) !== null) {
      return true;
    }
    return this.router.createUrlTree([signInFullRoute]);
  }
}
