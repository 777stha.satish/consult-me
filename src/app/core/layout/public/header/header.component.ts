import { Component, OnInit, Renderer2, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { ToggleAccountService } from './service/toggle-account.service';
import { AccountAction, SIGN_IN, SIGN_UP } from '@sharedTypes/custom-types';
import { Router } from '@angular/router';
import { RouteConstant } from '@sharedConstant/route-constant';
import { fromEvent, from } from 'rxjs';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  signInRoute = RouteConstant.signInFullRoute;
  signUpRoute = RouteConstant.signUpFullRoute;

  constructor(private toggleService: ToggleAccountService, private router: Router) { }

  ngOnInit(): void {

  }


  routeTo(link: string): void {
    this.router.navigate([link]).then(() => {
      this.toggleService.toggle();
    });
  }
}
