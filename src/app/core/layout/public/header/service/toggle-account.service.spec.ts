import { TestBed } from '@angular/core/testing';

import { ToggleAccountService } from './toggle-account.service';

describe('ToggleAccountService', () => {
  let service: ToggleAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToggleAccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
