import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AccountAction } from '@sharedTypes/custom-types';
@Injectable({
  providedIn: 'root'
})
export class ToggleAccountService {
  private toggleAccount = new Subject<AccountAction>();
  constructor() { }

  listen(): Observable<AccountAction> {
    return this.toggleAccount.asObservable();
  }

  toggle() {
    this.toggleAccount.next();
  }
}
