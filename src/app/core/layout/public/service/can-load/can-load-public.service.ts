import { Injectable } from '@angular/core';
import { CanLoad, Router, Route, UrlTree } from '@angular/router';
import { RouteConstant } from '@sharedConstant/route-constant';
import { localStorageConstant } from '@sharedConstant/local-storage-constant';

const { key } = localStorageConstant;
const { accessToken } = key;
const { securedPageInitials } = RouteConstant;

@Injectable({
  providedIn: 'root'
})
export class CanLoadPublicService implements CanLoad {
  constructor(private router: Router) { }
  canLoad(
    route: Route,
    segments: import('@angular/router').UrlSegment[]
  ):
    | boolean
    | UrlTree
    | import('rxjs').Observable<boolean | UrlTree>
    | Promise<boolean | import('@angular/router').UrlTree> {
    return this.isLoggedOut();
  }

  isLoggedOut(): boolean | UrlTree {
    if (!!localStorage.getItem(accessToken)) {
      return this.router.createUrlTree([securedPageInitials]);
    }
    return true;
  }
}
